<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('vendor/autoload.php');
use Pheanstalk\Pheanstalk;

class Phean_stalk{

	function __construct($ip=array())
	{		

		$default_ip = '127.0.0.1';

		ini_set('default_socket_timeout', '2592000'); //30days
		$temp_ip = isset($ip['ip'])?trim($ip['ip']):false;
		$port =  isset($ip['port'])?trim($ip['port']):false;

		if (!empty($temp_ip)) {
			$default_ip = $temp_ip;
		}

		$CI =& get_instance();

		if (!empty($port)) {
			$CI->phean = new Pheanstalk($default_ip, $port);
		}else{
			$CI->phean = new Pheanstalk($default_ip);
		}
		
	}
	
}
?>