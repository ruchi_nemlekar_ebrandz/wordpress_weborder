<?php

class Order_model extends CI_Model {

    
    function __construct() {
        parent::__construct();
     }
     
     function getOrderList() {

        $this->db->select('order_id, name,order_status');
        $this->db->from('orders');
        $this->db->where('order_status', 'Pending');
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        }  


    function updateOrder($orderID)
    {
       $data = array('order_status' => 'Completed');
        $this->db->where('order_id', $orderID);
        $this->db->update('orders', $data);
    }
        

    function getDomainDetails($order_id) {

        $this->db->select('clone.*');
        $this->db->from('clone_website clone');
        $this->db->join('orders o', 'o.order_id = clone.order_id', 'inner');
        $this->db->where('o.order_id', $order_id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
        }        
        
        
}
