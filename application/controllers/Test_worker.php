<?php

class Test_worker extends CI_Controller{
	
	private $pheanstalk;

	function __construct()
	{
		parent::__construct();
		error_reporting(-1);
		ini_set('display_errors', 'On');

		$this->load->library('phean_stalk');
		$this->pheanstalk = $this->phean;
	}


	function index()
	{
		$this->pheanstalk->watch("contact_form1");

		while ($job = $this->pheanstalk->reserve())
        {
        	$curl_data = json_decode($job->getData(), true);

// echo "<pre>";
//print_r($curl_data);

        	if($curl_data['lead_from'] == 'signup_v2')
        	{
        		$this->pheanstalk->delete($job);
        		continue;
        	}

        	$this->pheanstalk->bury($job);
        }	
	}
}