<?php

class Weborder_worker extends CI_Controller{
	
	private $pheanstalk;

	function __construct()
	{
		parent::__construct();
		error_reporting(-1);
		ini_set('display_errors', 'On');
		$this->load->library('phean_stalk');
		$this->load->model('order_model');
		$this->pheanstalk = $this->phean;
	}


	function index()
	{
		$this->pheanstalk->watch("web_order");

		while ($job = $this->pheanstalk->reserve())
        {
        	$curl_data = json_decode($job->getData(), true);

        	 if($curl_data['order_status'] == 'Pending')
        	 {
					$domain_result = $this->order_model->getDomainDetails($curl_data['order_id']);
	 				$cloneresult = shell_exec("sh ".$this->config->item('project_directory')."/wordpress_clone.sh " .$domain_result[0]['clone_id']. " " .$domain_result[0]['source_domain']. " " .$domain_result[0]['source_directory']. " " .$domain_result[0]['source_dbname']. " " .$domain_result[0]['source_user']. " " .$domain_result[0]['source_password']. " " .$domain_result[0]['target_domain']. " " .$domain_result[0]['target_directory']. " " .$domain_result[0]['target_dbname']. " " .$domain_result[0]['target_user']. " " .$domain_result[0]['target_password']);
     				print_r($cloneresult);
     				$result = $this->order_model->updateOrder($curl_data['order_id']);
     				$this->pheanstalk->delete($job);
	        	 	
	        		continue;
        	 }
        	  		$this->pheanstalk->bury($job);
        	
        }	
	}
}