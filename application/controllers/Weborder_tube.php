<?php
class Weborder_tube extends CI_Controller{
	

	private $pheanstalk;

	function __construct()
	{
		parent::__construct();
		$this->load->library('phean_stalk');
		$this->load->model('order_model');
		$this->pheanstalk = $this->phean;
	}

	function checkOrderStatus(){
		return $this->order_model->getOrderList();
	}

	function index()
	{
		$order_result  = $this->checkOrderStatus();
     	$this->pheanstalk->useTube('web_order')->put(json_encode($order_result[0]));
	}
}