#!/bin/bash -e

#To clear previous screens output
clear

echo "==================================================="
echo "Clone WordPress Script"
echo "==================================================="

#Set Source and Target website variables through arguments
clone_id=$1
source_domain=$2
source_directory=$3
source_dbname=$4
source_dbuser=$5
source_dbpass=$6
target_domain=$7
target_directory=$8
target_dbname=$9
target_dbuser=${10}
target_dbpass=${11}

#Set Default Settings
mysql_host="localhost"
mysql_user="root"
mysql_pass="ser_123"
mysql_dbname="worker_test"
clone_website_tablename="clone_website"
tar_compress_directory="/var/www/html/tarcompress"
search_replace_plugin_folder_name="search_replace"
search_replace_plugin_directory="/var/www/html/"$search_replace_plugin_folder_name
NOW=$(date +"%Y-%m-%d-%H%M")

#Print Source and Target website variables values
echo "Clone Website ID : "$clone_id
echo "MySQL Host Name : "$mysql_host
echo "MySQL Master Username : "$mysql_user
echo "MySQL Master Password : "$mysql_pass
echo "MySQL Master Database Name : "$mysql_dbname
echo "TAR Compression Directory : "$tar_compress_directory
echo "Search and Replace URL Plugin Directory : "$search_replace_plugin_directory
echo "Source Domain : "$source_domain
echo "Source Directory (no trailing slash) : "$source_directory
echo "Source Database Name : "$source_dbname
echo "Source Database Username : "$source_dbuser
echo "Source Database Password : "$source_dbpass
echo "Target Domain : "$target_domain
echo "Target Directory (no trailing slash) : "$target_directory
echo "Target Database Name : "$target_dbname
echo "Target Database Username : "$target_dbuser
echo "Target Database Password : "$target_dbpass

echo "==================================================="
echo "WordPress Cloning is Beginning"
echo "==================================================="
#backup source_directory
cd $source_directory
# add -v option to these if you want to see verbose file listings
tar -czf $tar_compress_directory/source_clone_$NOW.tar.gz .
#unzip clone in target directory
mkdir -p $target_directory
tar -xzf $tar_compress_directory/source_clone_$NOW.tar.gz -C $target_directory
#remove tarball of source
rm $tar_compress_directory/source_clone_$NOW.tar.gz
cd $target_directory
# Reset Directory Permissions
find $target_directory -type d -exec chmod 755 {} \;
find $target_directory -type f -exec chmod 644 {} \;
#update directory duplicated status into database
mysql -h $mysql_host  -u $mysql_user -p$mysql_pass $mysql_dbname -e "UPDATE $clone_website_tablename SET directory_duplicated = 'Yes' WHERE clone_id='$clone_id'";
echo "==================================================="
echo "Directory duplicated"
echo "==================================================="

#set database details with perl find and replace
perl -pi -e "s/$source_dbname/$target_dbname/g" wp-config.php
perl -pi -e "s/$source_dbuser/$target_dbuser/g" wp-config.php
perl -pi -e "s/$source_dbpass/$target_dbpass/g" wp-config.php
#update config updated status into database
mysql -h $mysql_host  -u $mysql_user -p$mysql_pass $mysql_dbname -e "UPDATE $clone_website_tablename SET config_updated = 'Yes' WHERE clone_id='$clone_id'";
echo "==================================================="
echo "Wordpress Config duplicated"
echo "==================================================="

# Begin Database Duplication
# Export the database
mysqldump -u$mysql_user -p$mysql_pass $source_dbname > $target_directory/clone_$NOW.sql
# Create the target database and permissions
mysql -u$mysql_user -p$mysql_pass -e "create database $target_dbname; GRANT ALL PRIVILEGES ON $target_dbname.* TO '$target_dbuser'@'localhost' IDENTIFIED BY '$target_dbpass'"
# Import the source database into the target
mysql -u$mysql_user -p$mysql_pass $target_dbname < $target_directory/clone_$NOW.sql
#remove SQL exported DB
rm $target_directory/clone_$NOW.sql
#update database duplicated status into database
mysql -h $mysql_host  -u $mysql_user -p$mysql_pass $mysql_dbname -e "UPDATE $clone_website_tablename SET database_duplicated = 'Yes' WHERE clone_id='$clone_id'";
echo "==================================================="
echo "Database duplicated"
echo "==================================================="

#Copy Search and Replace plugin into target directory
cp -R $search_replace_plugin_directory $target_directory
php $target_directory"/"$search_replace_plugin_folder_name"/"srdb.cli.php -h $mysql_host -n $target_dbname -u $target_dbuser -p $target_dbpass -s $source_domain -r $target_domain
rm -fr $target_directory"/"$search_replace_plugin_folder_name
#update url updated status into database
mysql -h $mysql_host  -u $mysql_user -p$mysql_pass $mysql_dbname -e "UPDATE $clone_website_tablename SET url_updated = 'Yes' WHERE clone_id='$clone_id'";
echo "==================================================="
echo "URL updated"
echo "==================================================="

#update clone website status into database
mysql -h $mysql_host  -u $mysql_user -p$mysql_pass $mysql_dbname -e "UPDATE $clone_website_tablename SET clone_website_status = 'Yes' WHERE clone_id='$clone_id'";
echo "==================================================="
echo "Clone is complete."
echo "Test at http://"$target_domain
echo "==================================================="